## [Wake Up Honey](https://www.canva.com/design/DACLodDutWw/b1nEZTjToX8TV0BbdXljSA/view?website)

### Lip Sync Instructions

A single short puff will activate the alarm. The puff and sip acts as a button for the SOS remote that activates the vibrating alarm. 

### iOS App Interface Setup

These steps will walk you through the setup for the iOS interface for the wake up device. Images meant to guide you are included in the "Images" folder and are numbered to match these written instructions.

1. Download the "Blynk - Arduino, ESP8266, RPi” app by Blynk, Inc. from the App Store.

2. Open the app and create a new account or log-in with Facebook. (You will receive 2000 Energy at sign-up. This is enough for our purposes; there is no need to buy more Energy.)

3. Load the “Wake Up Honey” interface by scanning the QR code “3. QR Code - Wake Me Up Honey” that is included in the images folder.

4. The interface will appear. Press the play button (triangle) in the upper right-hand corner to run the app.

5. The app is now ready to use. The large button labelled “Sleep Well” can be pressed to activate the alarm. The “Sleep Well” button will become a red “Emergency” button when it has been activated. The LED on the right will light up as a secondary confirmation of activation.

6. Press the large "Emergency" button again to turn off the alarm. The button label will return to "Sleep Well."

### Need Help? ###

Contact Us:

bikram.robotics@gmail.com
cecilia_liu@hotmail.ca
kewima@gmail.com
nikhil.prakash1995@gmail.com