// This #include statement was automatically added by the Particle IDE.
#include <blynk.h>
// -----------------------------------
// Wake Up Honey
// -----------------------------------


int actuator = D7;
WidgetLED led_feedback(V1);
//DANGER - DO NOT SHARE!!!!
char auth[] = "92a1bc2b3ef24ae191c3df89ec3c95cb"; // Put your blynk token here
//DANGER - DO NOT SHARE!!!!


void setup()
{
   Blynk.begin(auth);
   pinMode(actuator, OUTPUT);
   digitalWrite(actuator, LOW);
}

long loop_timeout = 1000; // milliseconds
long current_time = 0;
long prev_time = 0;
void loop()
{
 Blynk.run();
 current_time = millis();
 if(current_time - prev_time > loop_timeout)
   {
      
     if(digitalRead(actuator))
      {
            led_feedback.setValue(255);
      }
      else
      {
           led_feedback.setValue(0);
      }
      
      prev_time = millis();
    }
}



